﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FitnessappManager
{
    public partial class NeueUebung : Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                //everything in here fires only the first time your page is loaded
                if (Session["user"] != null)
                {
                    if (Request.QueryString["bezeichnung"] != null && Request.QueryString["beschreibung"] != null && Request.QueryString["bildurl"] != null)//ist null wenn neue Übung angeklickt wird. Kein null wenn bestehende Übung editiert wird.
                    {
                        tbBezeichnung.Text = Request.QueryString["bezeichnung"].ToString();
                        tbBeschreibung.Text = Request.QueryString["beschreibung"].ToString();
                    }
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                //reload your data grid here ad everytime you post back it will be forced to update(assuming your changes
                //were saved to your datasource)
            }

        }

        protected string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        protected void btnUpdload_Click(object sender, EventArgs e)
        {
            String bezeichnung = tbBezeichnung.Text;
            String beschreibung = tbBeschreibung.Text;
            String studioid = Session["studioid"].ToString();

            //Bild hochladen
            Boolean fileOK = false;
            String path = Server.MapPath("~/UploadedImages/");
            if (FileUpload.HasFile)
            {
                String fileExtension =
                    System.IO.Path.GetExtension(FileUpload.FileName).ToLower();
                String[] allowedExtensions =
                    {".gif", ".png", ".jpeg", ".jpg"};
                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        fileOK = true;
                    }
                }
            }
            if (fileOK)
            {
                try
                {
                    FileUpload.PostedFile.SaveAs(path + FileUpload.FileName);
                    lbUploadMessage.Text = "File uploaded!";
                    lbUploadMessage.Visible = true;
                    
                    //qrCode zeichnen
                    Zen.Barcode.CodeQrBarcodeDraw qrCode = Zen.Barcode.BarcodeDrawFactory.CodeQr;
                    System.Drawing.Image img = qrCode.Draw("http://greenzone.bplaced.net/db_getUebung.php?bezeichnung=" + bezeichnung, 1000);//TODO stürzt bei Umlauten ab
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, ImageFormat.Gif);
                    var base64Data = Convert.ToBase64String(ms.ToArray());
                    imgctrl.Src = "data:image/gif;base64," + base64Data;

                    //in Datenbank eintragen
                    using (WebClient wc = new WebClient())
                    {
                        string URI = "http://greenzone.bplaced.net/Manager/db_addUebung.php";
                        string myParameters = "&bezeichnung=" + bezeichnung + "&beschreibung=" + beschreibung + "&bildurl=" + FileUpload.FileName + "&studioid=" + studioid;
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        string HtmlResult = wc.UploadString(URI, myParameters);
                    }

                }
                catch (Exception ex)
                {
                    lbUploadMessage.Text = "File could not be uploaded.";
                    lbUploadMessage.Visible = true;
                }
            }
            else
            {
                lbUploadMessage.Text = "Cannot accept files of this type.";
                lbUploadMessage.Visible = true;
            }



        }
    }
}



