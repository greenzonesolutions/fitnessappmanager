﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FitnessappManager
{
    public partial class User : Page
    {
        JObject o;
        protected void Page_Load(object sender, EventArgs e)
        {
            initTabelle();
            if (!(Page.IsPostBack))
            {
                //everything in here fires only the first time your page is loaded
                if (Session["user"] != null)
                {
                   
                   
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                //reload your data grid here ad everytime you post back it will be forced to update(assuming your changes
                //were saved to your datasource)
            }
        }

        protected void initTabelle()
        {
            using (var client = new WebClient())
            {
                var result = client.DownloadString(string.Format("http://greenzone.bplaced.net/Manager/db_getUser.php?name="+ Session["studioname"].ToString()));

                o = JObject.Parse(result);
                dynamic dynJson = JsonConvert.DeserializeObject(result);

                DataTable table = new DataTable();
                DataColumn column;
                DataRow row;
                DataView view;

                // Create first column.
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "vorname";
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "nachname";
                table.Columns.Add(column);

                // Create third column.
                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "email";
                table.Columns.Add(column);


                for (int i = 0; i < o.Count / 3; i++) //geteilt durch die Anzahl an Spalten in der DB
                {
                    row = table.NewRow();
                    row["vorname"] = (string)o["vorname" + i.ToString()];
                    row["nachname"] = (string)o["nachname" + i.ToString()];
                    row["email"] = (string)o["email" + i.ToString()];
                    table.Rows.Add(row);
                }
                // Create a DataView using the DataTable.
                view = new DataView(table);
                gvUser.DataSource = view;
                gvUser.DataBind();
            }
        }

        protected void gvUser_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Werte aus Json holen und in NeueUebung zum bearbeiten übergeben.
            String email = (string)o["email" + e.NewEditIndex.ToString()];
            Response.Redirect("~/Fitnessstudio/Trainingsplaene.aspx?email=" + email);
        }
    }
}

