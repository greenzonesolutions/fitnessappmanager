﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FitnessappManager
{
    public partial class Trainingsplaene : Page
    {
        JObject o;
        string email;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                //everything in here fires only the first time your page is loaded
                if (Session["user"] != null)
                {
                    email = Request.QueryString["email"].ToString();
                    initTabelle();
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                //reload your data grid here ad everytime you post back it will be forced to update(assuming your changes
                //were saved to your datasource)
            }
        }

        protected void initTabelle()
        {
            using (var client = new WebClient())
            {
                var result = client.DownloadString(string.Format("http://greenzone.bplaced.net/db_getTrainingsplan.php?email=" + email));

                if(result !="[]")//Trainingspläne vorhanden
                { 
                    o = JObject.Parse(result);
                    dynamic dynJson = JsonConvert.DeserializeObject(result);

                    DataTable table = new DataTable();
                    DataColumn column;
                    DataRow row;
                    DataView view;

                    // Create first column.
                    column = new DataColumn();
                    column.DataType = System.Type.GetType("System.String");
                    column.ColumnName = "Trainingsplan";
                    table.Columns.Add(column);


                    for (int i = 0; i < o.Count / 1; i++) //geteilt durch die Anzahl an Spalten in der DB
                    {
                        row = table.NewRow();
                        row["Trainingsplan"] = (string)o["trainingsplanname" + i.ToString()];
                        table.Rows.Add(row);
                    }
                    // Create a DataView using the DataTable.
                    view = new DataView(table);
                    gvTrainingsplaene.DataSource = view;
                    gvTrainingsplaene.DataBind();
                }
            }
        }

        protected void gvTrainingsplaene_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

    }
}

