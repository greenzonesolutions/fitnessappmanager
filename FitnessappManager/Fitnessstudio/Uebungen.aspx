﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Uebungen.aspx.cs" Inherits="FitnessappManager.Uebungen" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
                <p>

            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Übungen"></asp:Label>

        </p>
        <p>

            <asp:Label ID="Label2" runat="server" Font-Size="12pt" Text="Aktuelle Übungen dieses Fitnessstudios"></asp:Label>

            :</p>
            <asp:GridView ID="gvUebungenIntern" runat="server" AutoGenerateColumns="False" AutoGenerateEditButton="True" OnRowEditing="gvUebungen_RowEditing" OnSelectedIndexChanged="gvUebungen_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField HeaderText="Bezeichnung" DataField="Bezeichnung"></asp:BoundField>
                    <asp:BoundField HeaderText="Beschreibung" DataField="Beschreibung"></asp:BoundField>
                    <asp:ImageField DataImageUrlField="PictureURL" ControlStyle-Width="120" ControlStyle-Height = "120">

<ControlStyle Height="120px" Width="120px"></ControlStyle>

                    </asp:ImageField>
                </Columns>
            </asp:GridView>
                <p>
                    &nbsp;</p>
                <p>

            <asp:Label ID="Label3" runat="server" Font-Size="12pt" Text="freie Übungen:"></asp:Label>

        </p>
                <p>
            <asp:GridView ID="gvUebungenGlobal" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField HeaderText="Bezeichnung" DataField="Bezeichnung"></asp:BoundField>
                    <asp:BoundField HeaderText="Beschreibung" DataField="Beschreibung"></asp:BoundField>
                    <asp:ImageField DataImageUrlField="PictureURL" ControlStyle-Width="120" ControlStyle-Height = "120">

<ControlStyle Height="120px" Width="120px"></ControlStyle>

                    </asp:ImageField>
                </Columns>
            </asp:GridView>
        </p>
                <p>
                    &nbsp;</p>
                <p>
                    &nbsp;</p>
        <p>
            <asp:Button ID="btNeueUebung" runat="server" OnClick="btNeueUebung_Click" Text="Neue Übung anlegen" Visible="False" />
        </p>
        <p>
            &nbsp;</p>

        <p>
            &nbsp;</p>
    </div>

    <div class="row">
    </div>

</asp:Content>
