﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FitnessappManager
{
    public partial class Infos : Page
    {
        JObject o;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                //everything in here fires only the first time your page is loaded
                if (Session["user"] != null)
                {
                    initTextBox();
                }
                else
                {
                   Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                //reload your data grid here ad everytime you post back it will be forced to update(assuming your changes
                //were saved to your datasource)
            }
           
        }

        protected void initTextBox()
        {
            using (var client = new WebClient())
            {
                var result = client.DownloadString(string.Format("http://greenzone.bplaced.net/Manager/db_getInfotext.php?name=Clever Fit"));

                o = JObject.Parse(result);
                dynamic dynJson = JsonConvert.DeserializeObject(result);

                tbInfoText.Text = (string)o["infotext0"];

            }
 
        }

        protected void Update_Clicked(object sender, EventArgs e)
        {
            using (var client = new WebClient())
            {
                string infotext = tbInfoText.Text;
                string asdf = "http://greenzone.bplaced.net/Manager/db_updateInfotext.php?name=Clever Fit&infotext=" + infotext;
                var result = client.DownloadString(string.Format("http://greenzone.bplaced.net/Manager/db_updateInfotext.php?name=Clever Fit&infotext=" + infotext));
                Response.Redirect("~/Fitnessstudio/Infos.aspx");
            }
            //Response.Redirect("NeueUebung.aspx");
        }

    }
}

