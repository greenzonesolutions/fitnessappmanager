﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Trainingsplaene.aspx.cs" Inherits="FitnessappManager.Trainingsplaene" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
                <p>

            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Trainingsplaene"></asp:Label>

        </p>
        <p>

            <asp:Label ID="Label2" runat="server" Font-Size="12pt" Text="Trainingsplaene"></asp:Label>

        </p>
                <p>

                    &nbsp;</p>
        <p>
            <asp:GridView ID="gvTrainingsplaene" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField HeaderText="Trainingsplan" DataField="Trainingsplan"></asp:BoundField>
                </Columns>
            </asp:GridView>
        </p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>

        <p>
            &nbsp;</p>
    </div>

    <div class="row">
    </div>

</asp:Content>
