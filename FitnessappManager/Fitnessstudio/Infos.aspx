﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Infos.aspx.cs" Inherits="FitnessappManager.Infos" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <p>

            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Infotext"></asp:Label>

        </p>
        <p>

            <asp:Label ID="Label2" runat="server" Font-Size="12pt" Text="Text der in der App angezeigt wird, wenn User auf &quot;Info&quot; klickt. Z.B. Öffnungszeiten"></asp:Label>

        </p>
        <p>

            &nbsp;</p>
        <p>

            <asp:TextBox ID="tbInfoText" runat="server" Height="150px" Width="472px"></asp:TextBox>

        </p>
        <p>
            <asp:Button ID="btNeueUebung" runat="server" Text="Updaten" Visible="True" OnClick="Update_Clicked" />
        </p>

        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>

    </div>

    <div class="row">
    </div>

</asp:Content>
