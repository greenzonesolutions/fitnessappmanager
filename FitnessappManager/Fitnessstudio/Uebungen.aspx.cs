﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FitnessappManager
{
    public partial class Uebungen : Page
    {
        JObject o;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!(Page.IsPostBack))
            {
                //everything in here fires only the first time your page is loaded
                if (Session["user"] != null)
                {
                    btNeueUebung.Visible = true;
                    initTabelleGlobal();
                    initTabelleIntern();
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                //reload your data grid here ad everytime you post back it will be forced to update(assuming your changes
                //were saved to your datasource)
            }
        }

        protected void initTabelleGlobal()
        {
            using (var client = new WebClient())
            {
                var result = client.DownloadString(string.Format("http://greenzone.bplaced.net/Manager/db_getUebungen.php?studioid=0"));

                o = JObject.Parse(result);
                dynamic dynJson = JsonConvert.DeserializeObject(result);

                DataTable table = new DataTable();
                DataColumn column;
                DataRow row;
                DataView view;

                // Create first column.
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "bezeichnung";
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "beschreibung";
                table.Columns.Add(column);

                // Create third column.
                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "PictureURL";
                table.Columns.Add(column);


                for (int i = 0; i < o.Count / 3; i++) //geteilt durch die Anzahl an Spalten in der DB
                {
                    row = table.NewRow();
                    row["bezeichnung"] = (string)o["bezeichnung" + i.ToString()];
                    row["beschreibung"] = (string)o["beschreibung" + i.ToString()];
                    row["PictureURL"] = ResolveUrl((string)o["bildurl" + i.ToString()]);
                    table.Rows.Add(row);
                }
                // Create a DataView using the DataTable.
                view = new DataView(table);
                gvUebungenGlobal.DataSource = view;
                gvUebungenGlobal.DataBind();
            }
        }

        protected void initTabelleIntern()
        {
            using (var client = new WebClient())
            {
                var result = client.DownloadString(string.Format("http://greenzone.bplaced.net/Manager/db_getUebungen.php?studioname="+ Session["studioname"].ToString()));

                o = JObject.Parse(result);
                dynamic dynJson = JsonConvert.DeserializeObject(result);

                DataTable table = new DataTable();
                DataColumn column;
                DataRow row;
                DataView view;

                // Create first column.
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "bezeichnung";
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "beschreibung";
                table.Columns.Add(column);

                // Create third column.
                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "PictureURL";
                table.Columns.Add(column);


                for (int i = 0; i < o.Count / 3; i++) //geteilt durch die Anzahl an Spalten in der DB
                {
                 
                    row = table.NewRow();
                    row["bezeichnung"] = (string)o["bezeichnung" + i.ToString()];
                    row["beschreibung"] = (string)o["beschreibung" + i.ToString()];

                    if(o["bildurl" + i.ToString()].ToString().Contains("www" ))
                    {
                        row["PictureURL"] = ResolveUrl((string)o["bildurl" + i.ToString()]);
                    }
                    else
                    {
                        row["PictureURL"] = "~/UploadedImages/"+ (string)o["bildurl" + i.ToString()];
                    }
                   
                    table.Rows.Add(row);
                }
                // Create a DataView using the DataTable.
                view = new DataView(table);
                gvUebungenIntern.DataSource = view;
                gvUebungenIntern.DataBind();
            }
        }

        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView dr = (DataRowView)e.Row.DataItem;
                string imageUrl = "data:image/jpg;base64," + Convert.ToBase64String((byte[])dr["Data"]);
                (e.Row.FindControl("Image1") as System.Web.UI.WebControls.Image).ImageUrl = imageUrl;
            }
        }

        protected void gvUebungen_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Werte aus Json holen und in NeueUebung zum bearbeiten übergeben.
            String bezeichnung = (string)o["bezeichnung" + e.NewEditIndex.ToString()];
            String beschreibung = (string)o["beschreibung" + e.NewEditIndex.ToString()];
            String bildurl = (string)o["bildurl" + e.NewEditIndex.ToString()];
            Response.Redirect("~/Fitnessstudio/NeueUebung.aspx?bezeichnung=" + bezeichnung+ "&beschreibung=" + beschreibung + "&bildurl=" + bildurl);
        }

        protected void btNeueUebung_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Fitnessstudio/NeueUebung");
        }

        protected void gvUebungen_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

