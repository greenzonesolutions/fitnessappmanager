﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="FitnessappManager.User" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
                <p>

            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="User"></asp:Label>

        </p>
        <p>

            <asp:Label ID="Label2" runat="server" Font-Size="12pt" Text="Aktuell registrierte Benutzer:"></asp:Label>

        </p>
                <p>

                    &nbsp;</p>
        <p>
            <asp:GridView ID="gvUser" runat="server" AutoGenerateColumns="False" AutoGenerateEditButton="True" OnRowEditing="gvUser_RowEditing">
                <Columns>
                    <asp:BoundField HeaderText="Vorname" DataField="Vorname"></asp:BoundField>
                    <asp:BoundField HeaderText=" Nachname" DataField="Nachname"></asp:BoundField>
                    <asp:BoundField DataField="email" HeaderText="E-Mail" />
                </Columns>
            </asp:GridView>
        </p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>

        <p>
            &nbsp;</p>
    </div>

    <div class="row">
    </div>

</asp:Content>
