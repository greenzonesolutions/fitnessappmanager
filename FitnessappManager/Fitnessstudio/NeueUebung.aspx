﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NeueUebung.aspx.cs" Inherits="FitnessappManager.NeueUebung" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
                <p>

            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Neue Übung anlegen"></asp:Label>

        </p>
        <p>

            <asp:Label ID="Label2" runat="server" Font-Size="12pt" Text="möchten Sie diesem Studio eine weitere Übung hinzufügen?"></asp:Label>

        </p>
        <p>&nbsp;</p>
        <p>Bezeichnung:&nbsp;&nbsp;
            <asp:TextBox ID="tbBezeichnung" runat="server"></asp:TextBox>
        </p>
        <p>Beschreibung:&nbsp;&nbsp;
            <asp:TextBox ID="tbBeschreibung" runat="server"></asp:TextBox>
        </p>
        <asp:FileUpload ID="FileUpload" runat="server" />
        <asp:Button ID="btnUpdload" runat="server" Text ="Upload" OnClick="btnUpdload_Click" />
        <p>
            <asp:Label ID="lbUploadMessage" runat="server" Text="Label" Visible="False"></asp:Label>
                </p>
        <p>
            &nbsp;</p>
        <p>
            <img src="data:image/gif;base64,<YOUR BASE64 DATA>" width="100" height="100" id="imgctrl" runat="server"/>
        </p>

        <p>
            &nbsp;</p>
    </div>

    <div class="row">
    </div>

</asp:Content>
