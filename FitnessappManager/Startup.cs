﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FitnessappManager.Startup))]
namespace FitnessappManager
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
